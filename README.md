This is the repository for the Actor Tester that I (Sam Taggart) developed and demoed at the 2018 ECLA Conference in Madrid.

Basically it provides tools to automatically create a tester actor that acts a test harness for the actor you want to test (Actor Under Test or AUT)

When the vipm package is installed these tools will show up in the Tools Menu.

To build the package:
The vipb file in the source directory.

Unit testing:
You can safely ignore the vi tester folder.  It was my attempting at using unit testing for the scripting functions, but it was too cumbersome dealing with all the different app instances. If you can figureout a way to make 8it work, please go ahead.  I just invested way too much time and realized it was going nowhere.  Maybe someone else will have better luck.

Manual testing:
There is a folder called "code for testing" in the 1-cleanup branch.  Once that gets merged in, it has a project with a simple actor that uses all 3 types of messages, so you can run the scripting on that and look at the results as a quick sanity check. It's not a very thorough test, but its the best I came up with.

Contribution Guidelines:
For now, just simply fork it, make your changes.  Before you submit a merge request into master, run the vi analyzer tests.  The results don't have to be perfect (I don't have the parameters tweaked exactly the way I want them yet), but most tests should pass.  
